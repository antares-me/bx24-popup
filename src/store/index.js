import { createStore } from 'vuex';

export default createStore({
  state: () => {
    return {
      currentUserId: '',
      sortLinks: [
        {
          title: 'Название',
          width: '310px',
          code: 'NAME',
          type: 'desc',
          active: false,
        },
        {
          title: 'Размер',
          width: '79px',
          code: 'SIZE',
          type: 'desc',
          active: false,
        },
        {
          title: 'Кто изменил',
          width: '122px',
          code: 'UPDATED_BY',
          type: 'desc',
          active: false,
        },
        {
          title: 'Изменен',
          width: '90px',
          code: 'UPDATE_TIME',
          type: 'desc',
          active: true,
        },
      ],
      pathChain: [],
      multipleSelect: false,
      result: [],
      disks: [
        {
          title: 'Диски пользователей',
          type: 'user',
        },
        {
          title: 'Общие диски',
          type: 'common',
        },
        {
          title: 'Диски групп',
          type: 'group',
        },
      ],
      content: {
        folders: [],
        files: [],
      },
      selectedFiles: [],
    };
  },
  mutations: {
    setCurrentUserId(state, payload) {
      state.currentUserId = payload;
    },
    resetStorage(state) {
      //reset sort links
      state.sortLinks.forEach((link, index, array) => {
        link.type = 'desc';
        link.active = false;
        if (index === array.length - 1) {
          link.active = true;
        }
      });
      //reset path chain
      state.pathChain = [];
      //reset content
      state.content.folders = [];
      state.content.files = [];
      //reset selected files
      state.selectedFiles = [];
    },
    changeSortLinks(state, payload) {
      let index = state.sortLinks.findIndex((link) => {
        return link.code === payload.code;
      });

      if (payload.type) {
        state.sortLinks[index].type = payload.type;
      } else if (payload.active) {
        state.sortLinks.forEach((link) => {
          link.active = false;
        });
        state.sortLinks[index].active = true;
      }
    },
    setPathChain(state, payload) {
      if (payload.type === 'storage') {
        state.pathChain = [payload];
      } else if (payload.type === 'folder') {
        let index = state.pathChain.findIndex(
          (pathItem) =>
            pathItem.id === payload.id && pathItem.type === payload.type
        );
        if (index > 0) {
          //remove from the path
          state.pathChain.splice(index + 1);
        } else {
          //add to the path
          state.pathChain.push(payload);
        }
      }
    },
    setResult(state, payload) {
      state.result = payload;
    },
    setActiveDisk(state, payload) {
      state.result.forEach((disk) => {
        if (disk.ID === payload.ID) {
          disk.active = true;
        } else {
          disk.active = false;
        }
      });
    },
    setContent(state, payload) {
      state.content.folders = payload.filter((item) => {
        return item.TYPE === 'folder';
      });
      state.content.files = payload.filter((item) => {
        return (
          item.TYPE === 'file' &&
          item.NAME.substring(item.NAME.length - 3).toLowerCase() === 'pdf'
        );
      });
    },
    selectFile(state, payload) {
      if (state.multipleSelect) {
        state.selectedFiles.push(payload);
      } else {
        state.selectedFiles[0] = payload;
      }
    },
    deselectFile(state, payload) {
      let i;
      state.selectedFiles.forEach((fileObject, index) => {
        if (fileObject.id === payload) {
          i = index;
        }
      });
      if (i !== undefined) {
        state.selectedFiles.splice(i, 1);
      }
    },
  },
  actions: {
    getChildren({ commit }, payload) {
      commit('setContent', []);
      if (window.BX24) {
        window.BX24.callMethod(
          `disk.${payload.type}.getchildren`,
          { id: payload.id },
          (result) => {
            if (result.error()) {
              throw Error(result.error());
            } else {
              let content = result.data();
              commit('setContent', content);
            }
          }
        );
      }
    },
  },
});
